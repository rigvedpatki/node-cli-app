#!/usr/bin/env node

import program from 'commander';
import { prompt } from 'inquirer';
import { Customer, connection } from './index';
import {
  addCustomer,
  findCustomer,
  updateCustomer,
  removeCustomer,
  listAllCustomers
} from './controllers';
import { ICustomer } from './models';

// Customer Questions
const customerQuestions = [
  {
    type: 'input',
    name: 'firstName',
    message: 'Customer First Name : '
  },
  {
    type: 'input',
    name: 'lastName',
    message: 'Customer Last Name : '
  },
  {
    type: 'input',
    name: 'phone',
    message: 'Customer Phone Number : '
  },
  {
    type: 'input',
    name: 'email',
    message: 'Customer Email Address : '
  }
];

program.version('1.0.0').description('Client Management System');

/* program
  .command('add <firstName> <lastName> <phone> <email>')
  .alias('a')
  .description('Add a customer')
  .action(async (firstName, lastName, phone, email) => {
    await addCustomer(Customer, { firstName, lastName, phone, email });
    connection.close();
  }); */

program
  .command('add')
  .alias('a')
  .description('Add a customer')
  .action(async () => {
    const customer = await prompt(customerQuestions);
    await addCustomer(Customer, customer as ICustomer);
    connection.close();
  });

const findQuestion = [
  {
    type: 'input',
    name: 'name',
    message: 'Name : '
  }
];

/* program
  .command('find <name>')
  .alias('f')
  .description('Find customer')
  .action(async (name: string) => {
    await findCustomer(Customer, name);
    connection.close();
  }); */

program
  .command('find')
  .alias('f')
  .description('Find customers')
  .action(async () => {
    const answer = await prompt(findQuestion);
    await findCustomer(Customer, (answer as any).name);
    connection.close();
  });

const customerIdQuestion = [
  {
    type: 'input',
    name: 'id',
    message: 'Customer Id : '
  }
];

program
  .command('update')
  .alias('u')
  .description('Update a customer')
  .action(async () => {
    const idAnswer = await prompt(customerIdQuestion);
    const { id } = idAnswer as any;
    const customer = await prompt(customerQuestions);
    await updateCustomer(Customer, id, customer as ICustomer);
    connection.close();
  });

program
  .command('remove')
  .alias('r')
  .description('Removing a customer')
  .action(async () => {
    const idAnswer = await prompt(customerIdQuestion);
    const { id } = idAnswer as any;
    await removeCustomer(Customer, id);
    connection.close();
  });

program
  .command('list')
  .alias('ls')
  .description('List all customer')
  .action(async () => {
    await listAllCustomers(Customer);
    connection.close();
  });

program.parse(process.argv);
