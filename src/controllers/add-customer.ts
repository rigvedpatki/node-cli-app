import { Model } from 'mongoose';
import { ICustomer, ICustomerModel } from '../models';

export const addCustomer = async (
  Customer: Model<ICustomerModel>,
  customer: ICustomer
) => {
  const customerCreated = await Customer.create(customer);
  console.log(`Customer created : \n ${customerCreated}`);
};
