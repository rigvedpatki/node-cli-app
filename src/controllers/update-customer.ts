import { Model } from 'mongoose';
import { ICustomerModel, ICustomer } from '../models';

export const updateCustomer = async (
  Customer: Model<ICustomerModel>,
  _id: string,
  customer: ICustomer
) => {
  const updatedCustomer = await Customer.findOneAndUpdate({ _id }, customer, {
    new: true
  });

  console.log(`Updated Customer : \n${updatedCustomer}`);
};
