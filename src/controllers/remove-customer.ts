import { Model } from 'mongoose';
import { ICustomerModel, ICustomer } from '../models';

export const removeCustomer = async (
  Customer: Model<ICustomerModel>,
  _id: string
) => {
  const deletedCustomer = await Customer.findOneAndRemove({ _id });
  console.log(
    `Customer ${(deletedCustomer as ICustomer).firstName} ${
      (deletedCustomer as ICustomer).lastName
    } has been deleted`
  );
};
