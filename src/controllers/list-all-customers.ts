import { Model } from 'mongoose';
import { ICustomerModel } from '../models';

export const listAllCustomers = async (Customer: Model<ICustomerModel>) => {
  const customers = await Customer.find();
  console.log(
    `Number of customers : ${
      customers.length
    } \nList Of Customers : \n${JSON.stringify(customers, null, 2)}`
  );
};
