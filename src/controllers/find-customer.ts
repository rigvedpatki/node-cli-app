import { Model } from 'mongoose';
import { ICustomerModel } from '../models';

export const findCustomer = async (
  Customer: Model<ICustomerModel>,
  name: string
) => {
  const search = new RegExp(name, 'i');
  const customer = await Customer.find({
    $or: [{ firstName: search }, { lastName: search }]
  });

  console.log(
    `Matches: ${customer.length} \nCustomers found : \n${JSON.stringify(
      customer,
      null,
      2
    )}`
  );
};
