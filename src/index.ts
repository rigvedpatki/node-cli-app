import mongoose from 'mongoose';
import config from './config';
import { customerSchema, ICustomerModel } from './models';

// connect to db
export const connection = mongoose.createConnection(config.mongodb.uri, {
  useNewUrlParser: true
});

mongoose.set('useFindAndModify', false);

export const Customer = connection.model<ICustomerModel>(
  'Customer',
  customerSchema
);
