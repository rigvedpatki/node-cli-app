import { Schema, Document, model } from 'mongoose';

export interface ICustomer {
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
}

export interface ICustomerModel extends ICustomer, Document {}

export const customerSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  phone: { type: String, required: true },
  email: { type: String, required: true }
});
